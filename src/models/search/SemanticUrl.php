<?php

namespace werewolf8904\cmsdburl\models\search;


use yii\data\ActiveDataProvider;


/**
 * Redirects represents the model behind the search form about `common\modules\redirects\models\Redirects`.
 */
class SemanticUrl extends \werewolf8904\cmsdburl\models\SemanticUrl
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['url', 'route'], 'string'],
            [['id'], 'integer'],
            ['is_fixed','boolean']
        ];
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_fixed'=>$this->is_fixed
        ]);

        $query->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'route', $this->route]);


        return $dataProvider;
    }
}
