<?php

namespace werewolf8904\cmsdburl\models;


use werewolf8904\cmscore\behaviors\CacheInvalidateBehavior;
use werewolf8904\cmsdburl\behaviors\UniqunesBehavior;
use werewolf8904\cmsdburl\models\query\SemanticUrlQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%semantic_url}}".
 *
 * @property string $url
 * @property string $route
 * @property string $id
 * @property string $is_fixed
 */
class SemanticUrl extends ActiveRecord
{
    public $delimiter = '/';
    /**
     * @var \yii\db\Transaction
     */
    protected $_transaction;

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return
            [
                'u' => [
                    'class' => UniqunesBehavior::class
                ],
                'cacheInvalidate' => [
                    'class' => CacheInvalidateBehavior::class,
                    'tags' => [self::class
                    ]

                ]
            ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%semantic_url}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['url', 'route', 'id'], 'required'],
            ['url', 'unique'],
            ['id', 'integer'],
            ['is_fixed','default','value' => 0],
            [['url'], 'string', 'max' => 255],
            [['route'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'url' => Yii::t('model_labels', 'Url'),
            'route' => Yii::t('model_labels', 'Controller'),
            'is_fixed' => Yii::t('model_labels', 'Is Fixed'),
            'id' => Yii::t('model_labels', 'ID')
        ];
    }

    /**
     * @inheritdoc
     * @return SemanticUrlQuery the active query used by this AR class.
     */
    public static function find(): SemanticUrlQuery
    {
        return new SemanticUrlQuery(static::class);
    }

    public function beforeSave($insert)
    {
        $this->_transaction = static::getDb()->beginTransaction();
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (array_key_exists('url', $changedAttributes)) {
            $old_url = $changedAttributes['url'];
            if ($old_url) {
                $condition = [
                    'and',
                    ['like', '{{%url}}', $this->getLike($old_url), false],
                    ['is_fixed'=>0],
                ];
                $substringExpr = $this->substringExpression(
                    '{{%url}}',
                    'LENGTH(:pathOld) + 1',
                    'LENGTH({{%url}}) - LENGTH(:pathOld)'
                );
                $update['url'] = new Expression($this->concatExpression([':pathNew', $substringExpr]));
                $params[':pathOld'] = $old_url;
                $params[':pathNew'] = $this->url;
                self::updateAll($update, $condition, $params);
            }
        }
        $this->_transaction->commit();
        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * @param string $path
     *
     * @return string
     */
    protected function getLike($path)
    {
        return strtr($path . $this->delimiter, ['%' => '\%', '_' => '\_', '\\' => '\\\\']) . '%';
    }

    protected function substringExpression($string, $from, $length)
    {
        if ($this->owner->getDb()->driverName === 'sqlite') {
            return "SUBSTR({$string}, {$from}, {$length})";
        }
        return "SUBSTRING({$string}, {$from}, {$length})";
    }

    /**
     * @param array $items
     *
     * @return string
     */
    protected function concatExpression($items)
    {
        if ($this->owner->getDb()->driverName === 'sqlite' || $this->owner->getDb()->driverName === 'pgsql') {
            return implode(' || ', $items);
        }
        return 'CONCAT(' . implode(',', $items) . ')';
    }
}
