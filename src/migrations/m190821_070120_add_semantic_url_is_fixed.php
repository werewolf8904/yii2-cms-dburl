<?php

use werewolf8904\cmscore\db\Migration;

class m190821_070120_add_semantic_url_is_fixed extends Migration
{
    public function safeUp()
    {

        $this->addColumn('{{%semantic_url}}','is_fixed',$this->tinyInteger(1)->notNull()->defaultValue(0)->after('url'));
        $this->createIndex('idx_su_is_fixed','{{%semantic_url}}','is_fixed');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_su_is_fixed','{{%semantic_url}}');
      $this->dropColumn('{{%semantic_url}}','is_fixed');
    }
}
