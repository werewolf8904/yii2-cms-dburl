<?php


use yii\db\Migration;

/**
 * Class m171024_221133_semantic_url
 */
class m171024_221133_semantic_url extends Migration
{
    /**
     * @return bool|void
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%semantic_url}}', [
            'url' => $this->string(1024)->notNull(),
            'route' => $this->string(50)->notNull(),
            'id' => $this->integer()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('url', '{{%semantic_url}}', 'url');
        $this->createIndex('route', '{{%semantic_url}}', ['route', 'id'], true);


    }

    /**
     * @return bool|void
     */
    public function down()
    {

        $this->dropTable('{{%semantic_url}}');

    }
}
