<?php

namespace werewolf8904\cmsdburl\components;

use werewolf8904\cmsdburl\models\SemanticUrl;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\caching\TagDependency;
use yii\web\UrlManager;
use yii\web\UrlNormalizer;
use yii\web\UrlRuleInterface;

/**
 * Class SlugUrlRuleDb
 *
 * @package common\modules\url\components
 */
class SlugUrlRuleDb extends BaseObject implements UrlRuleInterface
{
    private static $_mini_cache = [];
    private static $_dependency;
    public $normalizer;
    public $lazyLoad = [];

    public function init()
    {
        if (\is_array($this->normalizer)) {
            $normalizerConfig = array_merge(['class' => UrlNormalizer::class], $this->normalizer);
            $this->normalizer = Yii::createObject($normalizerConfig);
        }
        if ($this->normalizer !== null && $this->normalizer !== false && !$this->normalizer instanceof UrlNormalizer) {
            throw new InvalidConfigException('Invalid config for UrlRule::normalizer.');
        }

    }

    /**
     * @param UrlManager $manager
     * @param string $route
     * @param array $params
     *
     * @return bool|string
     */
    public function createUrl($manager, $route, $params)
    {
        $output = false;
        if (isset($params['semantic_url'])) {
            $output = $params['semantic_url'];
            unset($params['semantic_url']);
            return $output ? $output . ($manager->suffix ?: '') . (empty($params) ? '' : '?' . http_build_query($params)) : false;
        }
        if (isset($params['id'])) {
            if (array_key_exists($route . $params['id'], static::$_mini_cache)) {
                $output = static::$_mini_cache[$route . $params['id']];
            } else {
                $category = SemanticUrl::find()->andWhere(['id' => $params['id'], 'route' => $route])->select(['url'])->cache(3600, static::getDependency())->asArray()->one();
                if (!$category && array_key_exists($route, $this->lazyLoad) && ($product = $this->lazyLoad[$route]::findOne($params['id']))) {

                    $product->save(false);
                    $category = SemanticUrl::find()->andWhere(['id' => $params['id'], 'route' => $route])->select(['url'])->asArray()->one();

                }

                if ($category) {
                    static::$_mini_cache[$route . $params['id']] = $category['url'];
                }
                $output = $category ? $category['url'] : false;
            }
            unset($params['id']);

        }
        return $output ? $output . ($manager->suffix ?: '') . (empty($params) ? '' : '?' . http_build_query($params)) : false;
    }

    /**
     * @return TagDependency
     */
    protected static function getDependency()
    {
        if (static::$_dependency === null) {
            return static::$_dependency = new TagDependency(['tags' => SemanticUrl::class, 'reusable' => true]);
        }

        return static::$_dependency;
    }

    /**
     * @param UrlManager $manager
     * @param \yii\web\Request $request
     *
     * @return array|bool|mixed
     * @throws InvalidConfigException
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (array_key_exists($pathInfo, static::$_mini_cache)) {
            return static::$_mini_cache[$pathInfo];

        }
        $normalized = false;
        if ($this->hasNormalizer($manager)) {
            $pathInfo = $this->getNormalizer($manager)->normalizePathInfo($request->getPathInfo(), $manager->suffix, $normalized);
        } else {
            $pathInfo = $request->getPathInfo();
        }
        if (explode('/', $pathInfo)) {
            if ($manager->suffix) {
                $pathInfo = preg_replace('#' . $manager->suffix . '$#', '', $pathInfo);
            }
            $rez = SemanticUrl::find()
                ->select(['id', 'route', 'url'])
                ->where([
                    'or',
                    ['route' => trim($pathInfo, '/'), 'id' => $request->get('id'),],
                    ['url' => $pathInfo,],
                ])
                ->asArray()
                ->indexBy('url')
                ->one();
            if ($rez) {
                $out = [$rez['route']];
                $params = ['id' => $rez['id']];
                $out[] = $params;

                if ($normalized || ($rez['url'] !== $pathInfo)) {
                    // pathInfo was changed by normalizer - we need also normalize route
                    return $this->getNormalizer($manager)->normalizeRoute($out);
                }
                static::$_mini_cache[$pathInfo . $manager->suffix] = $out;
                return $out;
            }
        }

        return false;  // this rule does not apply
    }

    /**
     * @param UrlManager $manager the URL manager
     *
     * @return bool
     * @since 2.0.10
     */
    protected function hasNormalizer($manager)
    {
        return $this->getNormalizer($manager) instanceof UrlNormalizer;
    }

    /**
     * @param $manager
     *
     * @return mixed
     */
    protected function getNormalizer($manager)
    {
        if ($this->normalizer === null) {
            return $manager->normalizer;
        }

        return $this->normalizer;
    }
}