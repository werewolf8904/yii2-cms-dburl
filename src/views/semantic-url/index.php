<?php

use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use kartik\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \werewolf8904\cmsdburl\models\search\SemanticUrl */

$this->title = Yii::t('backend', 'Slugs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redirects-index">


    <p>
        <?php echo Html::a(Yii::t('backend', 'Create {modelClass}', [
            'modelClass' => 'Slugs',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            [
                'class' => EditableColumn::class,
                'attribute' => 'url',
                'vAlign' => 'middle',
                'contentOptions' => ['style' => 'max-width: 350px;'],
                'editableOptions' => [
                    'size' => 'lg',
                    'asPopover' => false,
//                'header' => 'Buy Amount',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    'placement' => 'auto',

                ],

            ],
            [
                'class' => EditableColumn::class,
                'attribute' => 'route',
                'vAlign' => 'middle',
                'contentOptions' => ['style' => 'max-width: 350px;'],
                'editableOptions' => [
                    'size' => 'lg',
                    'asPopover' => false,
//                'header' => 'Buy Amount',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    'placement' => 'auto',

                ],

            ],
            [
                'class' => EditableColumn::class,
                'attribute' => 'id',
                'vAlign' => 'middle',
                'contentOptions' => ['style' => 'max-width: 350px;'],
                'editableOptions' => [
                    'size' => 'lg',
                    'asPopover' => false,
//                'header' => 'Buy Amount',
                    'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,
                    'placement' => 'auto',

                ],

            ],
            [
                'class' => EditableColumn::class,
                'attribute' => 'is_fixed',
                'filter' => [
                    Yii::t('backend', '-'),
                    Yii::t('backend', '+')
                ],
                'vAlign' => 'middle',
                'contentOptions' => ['style' => 'max-width: 50px;'],
                'readonly' => function ($model, $key, $index, $widget) {
                    return (!(Yii::$app->user->can('manager') || Yii::$app->user->can('administrator'))); // do not allow editing of inactive records
                },
                'editableOptions' => [
                    'format' => Editable::FORMAT_BUTTON,
                    'displayValueConfig' => ['0' => GridView::ICON_INACTIVE, '1' => GridView::ICON_ACTIVE],
                    'inputType' => \kartik\editable\Editable::INPUT_SWITCH,
                    'placement' => 'auto',
                    'options' => [
                        'pluginOptions' => ['min' => 0, 'max' => 50000]
                    ]
                ]
            ],
            [
//                'header' => 'Actions',
                'class' => ActionColumn::class,
                'template' => '{update}{delete}',

            ],
        ],
    ]);
    ?>
</div>
