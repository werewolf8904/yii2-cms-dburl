<?php
/**
 * @var $this yii\web\View
 * @var $model \werewolf8904\cmsdburl\models\SemanticUrl
 */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Url',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Urls'), 'url' => ['index',],];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redirects-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
