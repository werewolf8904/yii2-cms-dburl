<?php

/* @var $this yii\web\View */
/* @var $model \werewolf8904\cmsdburl\models\SemanticUrl */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
        'modelClass' => 'Slugs',
    ]) . ' ' . $model->url;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Urls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->url];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="redirects-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
