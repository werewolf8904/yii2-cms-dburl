<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \werewolf8904\cmsdburl\models\SemanticUrl */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="redirects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'route')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'id')->textInput(['maxlength' => true]) ?>
    <?php echo $form->field($model, 'is_fixed')->checkbox() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
