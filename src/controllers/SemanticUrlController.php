<?php

namespace werewolf8904\cmsdburl\controllers;

use werewolf8904\cmscore\controllers\BackendController;
use werewolf8904\cmsdburl\models\search\SemanticUrl;


/**
 * ManageController implements the CRUD actions for Redirects model.
 */
class SemanticUrlController extends BackendController
{

    public $pk = 'url';
    public $searchClass = SemanticUrl::class;
    public $class = \werewolf8904\cmsdburl\models\SemanticUrl::class;


}
