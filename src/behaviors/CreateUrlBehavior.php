<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.02.2018
 * Time: 20:12
 */

namespace werewolf8904\cmsdburl\behaviors;


use werewolf8904\cmsdburl\models\SemanticUrl;
use werewolf8904\composite\service\ServiceEvent;
use yii\base\Behavior;
use yii\db\ActiveRecord;


/**
 * Class CreateUrlBehavior
 *
 * @package common\modules\url\behaviors
 */
class CreateUrlBehavior extends Behavior
{
    /**
     * @var \Closure
     */
    public $url_build_function;

    /**
     * @var array|string
     */
    public $generate_events = [ActiveRecord::EVENT_AFTER_INSERT,
        ActiveRecord::EVENT_AFTER_UPDATE];


    public $route;

    public $model_id = 'id';

    /**
     * @var string|\Closure
     */
    public $path_string = 'pathString';
    /**
     * @var array|string
     */
    public $delete_events = ActiveRecord::EVENT_AFTER_DELETE;


    /**
     * @return array
     */
    public function events(): array
    {
        $events = [];
        if (\is_string($this->generate_events)) {
            $events[$this->generate_events] = 'generate';
        }
        if (\is_array($this->generate_events)) {
            foreach ($this->generate_events as $k => $v) {
                $events[$v] = 'generate';
            }
        }

        if (\is_string($this->delete_events)) {
            $events[$this->delete_events] = 'delete';
        }
        if (\is_array($this->delete_events)) {
            foreach ($this->delete_events as $k => $v) {
                $events[$v] = 'delete';
            }
        }

        return $events;
    }

    /**
     * @param $event
     *
     * @throws \yii\db\Exception
     */
    public function generate($event): void
    {
        if ($event instanceof ServiceEvent) {
            $model = $event->model;
        } else {
            $model = $event->sender;
        }


        $slg = SemanticUrl::findOne(['route' => $this->route, 'id' => $model->{$this->model_id}]);
        $slg = $slg ?: new SemanticUrl(['route' => $this->route, 'id' => $model->{$this->model_id}]);
        if($slg->isNewRecord||!$slg->is_fixed)
        {
            if (\is_string($this->path_string)) {
                $slg->url = $model->{$this->path_string};
            } elseif (\is_callable($this->path_string)) {
                $slg->url = \call_user_func($this->path_string, [$model]);
            }
            $slg->save();
        }

    }

    /**
     * @param $event
     *
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function delete($event): void
    {
        $model = $event->sender;
        $slg = SemanticUrl::findOne(['route' => $this->route, 'id' => $model->{$this->model_id}]);
        if ($slg) {
            $slg->delete();
        }
    }


}