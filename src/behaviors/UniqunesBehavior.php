<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.09.2017
 * Time: 16:19
 */

namespace werewolf8904\cmsdburl\behaviors;

use yii\behaviors\SluggableBehavior;

/**
 * Class UniqunesBehavior
 *
 * @package common\modules\url\behaviors
 */
class UniqunesBehavior extends SluggableBehavior
{
    /**
     * @var string|array|null the attribute or list of attributes whose value will be converted into a slug
     * or `null` meaning that the `$value` property will be used to generate a slug.
     */
    public $attribute = 'url';
    public $slugAttribute = 'url';
    public $ensureUnique = true;

    /**
     * /**
     * @inheritdoc
     */
    protected function getValue($event)
    {
        if (!$this->isNewSlugNeeded()) {
            return $this->owner->{$this->slugAttribute};
        }
        $slug = $this->owner->{$this->slugAttribute};

        return $this->ensureUnique ? $this->makeUnique($slug) : $slug;
    }
}